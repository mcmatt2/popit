module.exports = function (grunt) {
    "use strict";

    var root = {
        js: [
            '../js/popit.js',
            '../js/PopIt/Component.js',
            '../js/{,**/}*.js'
        ]
    };

    grunt.initConfig({
        watch: {
            //options: { livereload: true },
            css: {
                files: '../css/{,**/}*.scss',
                tasks: 'sass:dev'
            },
            js: {
                files: root.js,
                tasks: 'concat:dev'
            }
        },
        sass: {
            options: {
                cacheLocation: 'cache',
                unixNewlines: true,
                loadPath: [
                    "C:/lib/ruby/lib/ruby/gems/1.9.1/gems/bourbon-3.1.8/app/assets/stylesheets",
                    "C:/lib/ruby/lib/ruby/gems/1.9.1/gems/neat-1.6.0/app/assets/stylesheets"
                ]
            },
            dev: {
                options: {
                    style: 'expanded',
                    debugInfo: true,
                    lineNumbers: true
                },
                src: '../css/popit.scss',
                dest: '../public/popit.min.css'
            },
            pub: {
                options: {
                    style: 'compressed',
                },
                src: '../css/popit.scss',
                dest: '../public/popit.min.css'
            }
        },
        concat: {
            options: {
                separator: ';'
            },
            dev: {
                src: root.js,
                dest: '../public/popit.min.js'
            },
            pub: {
                src: root.js,
                dest: 'cache/popit.min.js'
            }
        },
        uglify: {
            pub: {
                src: 'cache/popit.min.js',
                dest: '../public/popit.min.js'
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');

    grunt.registerTask('default', [ 'sass:pub', 'concat:pub', 'uglify:pub' ]);
    grunt.registerTask('dev', [ 'sass:dev', 'concat:dev' ]);
    grunt.registerTask('w', 'watch');
};
