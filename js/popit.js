var PopIt = {};

(function (global, undefined) {
    var $ = window.jQuery;

    $(document).ready(function () {
        var start = new Date(),
            last = new Date();

        var canvas = $('#game'),
            stage = new createjs.Stage("game");

        var game = new PopIt.SurvivalGame(canvas, stage);
        game.start();

        var intervalID = global.setInterval(function () {
            var now = new Date(),
                elapsedSeconds = ( now - last ) / 1000,
                totalSeconds = ( now - start ) / 1000;

            last = now;

            var scale = canvas.data('scale') || 1.0;
            stage.scaleX = scale;
            stage.scaleY = scale;

            game.update(elapsedSeconds, totalSeconds);
            game.draw(elapsedSeconds, totalSeconds);

            stage.update();
        }, 1000 / 60);
    });
})(window);

$(document).ready(function () {
    var delay = (function () {
        var timer = 0;
        return function (callback, duration) {
            window.clearTimeout(timer);
            timer = window.setTimeout(callback, duration);
        };
    })();

    var target = { width: 360, height: 640, ratio: 0 },
        parent = $('body'),
        canvas = $('#game');

    target.ratio = ( target.height / target.width );

    var resize = function () {
        var parentWidth = parent.width(),
            parentHeight = parent.height(),
            isVertical = ( parentWidth < parentHeight );

        var newHeight = parentHeight,
            newWidth = ( newHeight / target.ratio );

        canvas
            .css({
                top: ( parentHeight / 2 ) - ( newHeight / 2 ),
                left: ( parentWidth / 2 ) - ( newWidth / 2 )
            })
            .attr('height', newHeight)
            .attr('width', newWidth)
            .data('scale', ( newWidth / target.width ));
    };

    $(window).resize(function () {
        delay(resize, 100);
    }).trigger('resize');
});
