/**
 * @class
 */
PopIt.Component = (function (global, undefined) {
    "use strict";

    function Class(game) {
        var me = this;
        me.game = game;
    }

    Class.prototype.dispose = function () { };
    Class.prototype.update = function (elapsedSeconds, totalSeconds) { };
    Class.prototype.draw = function (elapsedSeconds, totalSeconds) { };

    return Class;
})(window);
