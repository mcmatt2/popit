/**
 * @class
 * @augments {PopIt.Component}
 */
PopIt.Balloon = (function (global, undefined) {
    "use strict";

    var base = PopIt.Component;
    Class.prototype = Object.create(base.prototype);
    function Class(game, number, onClickCallback) {
        var me = this;

        base.call(me, game);

        me.x = 0;
        me.y = 0;
        me.number = number;

        me.shapes = {
            circle: new createjs.Shape(),
            text: new createjs.Text(number, "20px Verdana", "white")
        };

        me.shapes.circle.graphics.beginFill('black').drawCircle(0, 0, 48);
        me.shapes.text.textAlign = 'center';
        me.shapes.text.textBaseline = 'middle';

        var stage = me.game.getStage();
        stage.addChild(me.shapes.circle);
        stage.addChild(me.shapes.text);

        me.shapes.circle.addEventListener('click', onClickCallback);
    }

    Class.prototype.update = function (elapsedSeconds, totalSeconds) {
        var me = this;

        me.x++;
        me.y += 2;

        me.shapes.text.x = me.x;
        me.shapes.text.y = me.y;
        me.shapes.circle.x = me.x;
        me.shapes.circle.y = me.y;

        base.prototype.update.call(me, elapsedSeconds, totalSeconds);
    };

    Class.prototype.dispose = function () {
        var me = this;

        var stage = me.game.getStage();
        stage.removeChild(me.shapes.circle);
        stage.removeChild(me.shapes.text);
    };

    return Class;
})(window);
