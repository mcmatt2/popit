/**
 * @class
 * @augments {PopIt.Game}
 */
PopIt.SurvivalGame = (function (global, undefined) {
    "use strict";

    var base = PopIt.Game;
    Class.prototype = Object.create(base.prototype);
    function Class(canvas, stage) {
        var me = this;

        base.call(me, canvas, stage);

        var number = 4;

        var balloon = new PopIt.Balloon(me, number, function () {
            balloon.dispose();
            me.components.remove(balloon);
        });
        me.components.add(balloon);
    }

    Class.prototype.update = function (elapsedSeconds, totalSeconds) {
        base.prototype.update.call(this, elapsedSeconds, totalSeconds);
    };

    Class.prototype.draw = function (elapsedSeconds, totalSeconds) {
        base.prototype.draw.call(this, elapsedSeconds, totalSeconds);
    };

    return Class;
})(window);
