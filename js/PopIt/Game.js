/**
 * @class
 */
PopIt.Game = (function (global, undefined) {
    "use strict";

    function Class(canvas, stage) {
        var me = this;

        me.canvas = canvas;
        me.stage = stage;

        me.tracking = {
            level: 0,
            target: 0,
            score: 0
        };

        me.timing = {
            speed: 100,
            tick: 0
        };

        me.components = [];
    }

    Class.prototype.getScale = function () {
        return this.canvas.data('scale') || 1.0;
    };

    Class.prototype.getStage = function () {
        return this.stage;
    };

    Class.prototype.start = function () {
        this.tracking.target = Math.round(Math.random() * ( 1200 - 400 ) + 400);
    };

    Class.prototype.update = function (elapsedSeconds, totalSeconds) {
        this.components.forEach(function (component) {
            component.update(elapsedSeconds, totalSeconds);
        });
    };

    Class.prototype.draw = function (elapsedSeconds, totalSeconds) {
        this.components.forEach(function (component) {
            component.draw(elapsedSeconds, totalSeconds);
        });
    };

    return Class;
})(window);
