# PopIt

A balloon-popping game written in JavaScript, using canvas.

## Building

Requires `npm` and `grunt`.

Go to the `.build` directory and install the following Grunt modules: `concat`, `uglify`, `sass`, `watch`.

Then run `grunt` to compile and minify assets.

## Usage

Visit `public/index.htm` in a modern web browser to launch the game.
